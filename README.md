# Web-ThirdParty-Unknown

Repository for hosting assets I use on my website whose license is unknown, to separate them from my own CC BY-SA 4.0 content.
